package com.gardockt.termuxterminalwidget.mainwidget;

import androidx.annotation.NonNull;

import com.gardockt.termuxterminalwidget.ColorScheme;

import lombok.Data;

@Data
public class MainWidgetPreferences {

    @NonNull
    private String command = "";
    private ColorScheme colorScheme = null;
    private Integer textSizeSp = null;
    private int refreshIntervalSecs = 15 * 60;
}
