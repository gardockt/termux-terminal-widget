package com.gardockt.termuxterminalwidget;

import androidx.annotation.NonNull;

public record ColorScheme(int colorForeground, int colorBackground) implements Cloneable {

    @NonNull
    @Override
    public ColorScheme clone() {
        try {
            return (ColorScheme) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
