package com.gardockt.termuxterminalwidget;

import androidx.annotation.NonNull;

import lombok.Data;

@Data
public class GlobalPreferences implements Cloneable {

    private ColorScheme colorScheme = new ColorScheme(0xFFFFFFFF, 0xBF000000);
    private int textSizeSp = 14;
    private boolean alarmManagerBackendEnabled = false;

    @NonNull
    @Override
    public GlobalPreferences clone() {
        try {
            GlobalPreferences clone = (GlobalPreferences) super.clone();
            clone.setColorScheme(colorScheme.clone());
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
